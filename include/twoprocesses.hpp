#ifndef TWOPROCESSES_HPP
#define TWOPROCESSES_HPP

SC_MODULE(TwoProcesses) {

    void wiper_thread();
    void blinker_thread();

    SC_CTOR(TwoProcesses) {
        SC_THREAD(wiper_thread);
        SC_THREAD(blinker_thread);
    }    

  private:
    bool blink;
  private:
    void wipe_left() const;
    void wipe_right() const;
    void blink_on();
    void blink_off();    
};

#endif
