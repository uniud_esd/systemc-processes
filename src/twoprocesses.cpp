#include <systemc.h>
#include <iostream>
#include "twoprocesses.hpp"

using namespace std;

void TwoProcesses::wipe_left() const {
  cout << "Wiping left at " << sc_time_stamp() << endl;
}

void TwoProcesses::wipe_right() const {
  cout << "Wiping right at " << sc_time_stamp() << endl;
}

void TwoProcesses::blink_on() {
  cout << "Blinking set to On at " << sc_time_stamp() << endl;
  blink = true;
}

void TwoProcesses::blink_off() {
  cout << "Blinking set to Off at " << sc_time_stamp() << endl;
  blink = false;
}

void TwoProcesses::wiper_thread() { 
    while(true) {
        wipe_left();
        wait(3,SC_SEC);
        wipe_right();
        wait(3,SC_SEC);
    }
}

void TwoProcesses::blinker_thread() { 
    while(true) {
        blink_on();
        wait(2,SC_SEC);
        blink_off();
        wait(2,SC_SEC);
    }
}
