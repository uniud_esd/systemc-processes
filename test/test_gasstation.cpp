//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//See gas_station.h for more information
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#include <systemc.h>
#include "gasstation.hpp"

unsigned errors = 0;
const char* simulation_name = "gas_station";

int sc_main(int argc, char* argv[]) 
{
  sc_set_time_resolution(1,SC_NS);
  
  GasStation station("station",10, 12, 1.5, 10);
  
  cout << "INFO: Starting gas station simulation" << endl;
  
  sc_start();
  
  cout << "INFO: Exiting gas_station simulation" << endl;
  cout << "INFO: Simulation " << simulation_name
       << " " << (errors?"FAILED":"PASSED")
       << " with " << errors << " errors"
       << endl;
  
  return errors?1:0;
}
