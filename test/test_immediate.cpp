#include <systemc.h>
#include <string>

using namespace std;

SC_MODULE(ImmediateProblem) {
    SC_CTOR(ImmediateProblem) {
        SC_THREAD(B_thread);
        SC_THREAD(A_thread);
        SC_THREAD(C_thread);
    }
    void A_thread() {
        a_event.notify(); // Immediate!
        cout << "A sent a_event!" << endl;

    }
    void B_thread() {
        wait(a_event);
        cout << "B got a_event!" << endl;
        b_received = true;
    }
    void C_thread() {
        wait(a_event);
        cout << "C got a_event!" << endl;
        c_received = true;
    }

    bool has_b_received() const { return b_received; }
    bool has_c_received() const { return c_received; }

  private:
    sc_event a_event;
    bool b_received, c_received;
};

int sc_main(int argc, char** argv) {

    ImmediateProblem problem("problem");

    sc_start();

    if (problem.has_c_received() && problem.has_b_received())
        return 0;

    return -1;
}
