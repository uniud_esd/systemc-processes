#include <systemc.h>
#include <string>

using namespace std;

SC_MODULE(OrderedEvents) {
    SC_CTOR(OrderedEvents) {
        SC_THREAD(B_thread);
        SC_THREAD(A_thread);
        SC_THREAD(C_thread);
    }
    void A_thread() {
        while (true) {
            a_event.notify(SC_ZERO_TIME);
            cout << "A sent a_event!" << endl;
            wait(c_event);
            cout << "A got c_event!" << endl;
        }
    }
    void B_thread() {
        while (true) {
            b_event.notify(SC_ZERO_TIME);
            cout << "B sent b_event!" << endl;
            wait(c_event);
            cout << "B got a_event!" << endl;
        }    
    }
    void C_thread() {
        while (true) {
            c_event.notify(SC_ZERO_TIME);
            cout << "C sent c_event!" << endl;
            wait(c_event);
            cout << "C got b_event!" << endl;
        }    
    }

  private:
    sc_event a_event, b_event, c_event;
};

int sc_main(int argc, char** argv) {

    OrderedEvents module("OrderedEvents");

    sc_start(20,SC_MS); // Note that execution does not stop, since simulated time does not pass!

    return 0;
}
