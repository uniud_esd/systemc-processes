#include <systemc.h>
#include <iostream>
#include "twoprocesses.hpp"

int sc_main(int argc, char* argv[]) {

    TwoProcesses twoProc("2Processes");

    sc_start(25,SC_SEC);

    return 0;
}
